import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import '../fungsi/navigator.dart';
import 'star_rating.dart';
import '../tampilan/product.dart';

import '../model/product.dart';

class Occasions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Nav.route(
            context,
            ProductPage(
              product: Product(
                  company: 'UC',
                  name: 'PUBG',
                  icon: 'assets/ab.jpg',
                  rating: 4.5,
                  remainingQuantity: 5,
                  price: 'Rp. 50.000'),
            ));
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 220,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Penawaran Menarik",
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.w600),
                        textAlign: TextAlign.start,
                      ),
                      Icon(
                        SimpleLineIcons.getIconData("screen-smartphone"),
                        color: Colors.black54,
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 6.0),
                    child: Divider(
                      color: Colors.black12,
                      height: 1,
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            border:
                                Border.all(width: 1, color: Colors.black12)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.asset('assets/ab.jpg',
                              height: 120,
                              width: MediaQuery.of(context).size.width / 3),
                        ),
                      ),
                      SizedBox(
                        width: 24,
                      ),
                      Container(
                        height: 142,
                        decoration: BoxDecoration(
                            border:
                                Border.all(width: 1, color: Colors.black12)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "02 : 03 : 04",
                                style: TextStyle(
                                  fontSize: 14,
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 4.0),
                                child: Text(
                                  "PUBG",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "RP. 50.000",
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 6,
                                  ),
                                  Text(
                                    'Rp.81.111',
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 9,
                                        decoration: TextDecoration.lineThrough),
                                  )
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 4.0),
                                child: StarRating(rating: 4, size: 10),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 4.0),
                                child: Text("Gratis Biaya Admin"),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 4.0),
                                child: Text(
                                  "Sisa 5 ",
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            elevation: 5,
          ),
        ),
      ),
    );
  }
}
